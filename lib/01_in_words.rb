class Fixnum
  ONES = {
    0 => "zero",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    }

  TEENS = {
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
  }
  TENS = {

    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety",
    100 => "hundred",


  }
  POWERS = {

    1000 => "thousand",
    1000000 => "million",
    1000000000 => "billion",
    1000000000000 => "trillion"
  }
  def chunk(string, size)
      string.scan(/.{1,#{size}}/)
  end

  def in_words
    num = self
    return "zero" if num == 0
    num_arr = []
    number = []
    chunk(num.to_s.reverse, 3).map {|el| num_arr << el.reverse}
    magnitude = num_arr.length
    num_arr.reverse_each do |el|
      num = el.to_i
      if num == 0
        magnitude -= 1
        next
      end
      until num == 0
        if num >= 100
          number << ONES[num/100]
          number << TENS[100]
          num -= num/100 * 100
        elsif (20..100).cover?(num)
          number << TENS[num / 10 * 10]
          num -= num /10 * 10
        elsif (10...20).cover?(num)
          number << TEENS[num]
          num -= num
        else (0...10).cover?(num)
          number << ONES[num]
          num -= num
        end
      end
      case magnitude
      when 2
        number << "thousand"
        magnitude -= 1
      when 3
        number << "million"
        magnitude -= 1
      when 4
        number << "billion"
        magnitude -= 1
      when 5
        number << "trillion"
        magnitude -= 1
      end
    end
  number.join(' ')
  end
end
